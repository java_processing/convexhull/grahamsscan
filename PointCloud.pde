class PointCloud extends ArrayList<SortedPoint> {

  protected long beginNanoTime;
  protected long endNanoTime;
  protected int edge = 50;
  protected int radius;

  PointCloud() {
    super();
  }
  PointCloud(Collection<SortedPoint> c) {
    super(c);
  }
  PointCloud(int nbP, int seed, EnumRandom typeRand) {
    PointCloudConstructor(nbP, new RandomGenerator(seed), typeRand);
  }
  PointCloud(int nbP, EnumRandom typeRand) {
    PointCloudConstructor(nbP, new RandomGenerator(), typeRand);
  }
  public void PointCloudConstructor(int nbP, RandomGenerator randGenerator, EnumRandom typeRand) {

    this.setBeginNanoTime();

    float X;
    float Y;

    radius = (width / 2) - edge;

    if (typeRand == EnumRandom.Uniform) {
      for (int i = 0; i < nbP; i++ ) {
        X = randGenerator.nextFloat()*radius;
        float YRange = sqrt((float) (Math.pow(radius, 2) - Math.pow(X, 2)));
        Y = randGenerator.nextFloat()*YRange;
        this.add(new SortedPoint(X, Y));
      }
    } else {
      for (int i = 0; i < nbP; i++ ) {
        X = (float) randGenerator.nextGaussian()*width/9;
        Y = (float) randGenerator.nextGaussian()*height/9;
        this.add(new SortedPoint(X, Y));
      }
    }

    this.setEndNanoTime();

    this.printDuration();
  }

  public void show(int strkW, int r, int g, int b) {
    strokeWeight(strkW);
    stroke(r, g, b);
    for (SortedPoint point : this) {
      point(point.x, point.y);
    }
  }

  public void connect (int i, int j) {
    SortedPoint a = this.get(i);
    SortedPoint b = this.get(j);
    line(a.x, a.y, b.x, b.y);
  }

  public void connectAll() {
    for (int i = 0; i < this.size(); i++) {
      try {
        this.connect(i, i+1);
      } 
      catch (IndexOutOfBoundsException e) {
        this.connect(i, 0);
      }
    }
  }

  public int noErrorSet(int indx, SortedPoint pv) {
    try {
      this.set(indx, pv);
      return indx;
    } 
    catch(IndexOutOfBoundsException e) {
      this.add(pv);
      return this.indexOf(pv);
    }
  }

  public SortedPoint getPoint(int i) {
    try {
      return this.get(i);
    } 
    catch (IndexOutOfBoundsException e) {
      return this.get(0);
    }
  }

  public void setBeginNanoTime() {
    this.beginNanoTime = System.nanoTime();
  }

  public void setEndNanoTime() {
    this.endNanoTime = System.nanoTime();
  }

  public long getBeginNanoTime() {
    return this.beginNanoTime;
  }

  public long getEndNanoTime() {
    return this.endNanoTime;
  }

  public void printDuration() {
    println("Duration of random set of points generation (us): " + this.getNanoDuration() / 1000);
  }

  public long getNanoDuration() {
    return this.endNanoTime - this.beginNanoTime;
  }
}
