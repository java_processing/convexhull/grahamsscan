abstract class Step {

  protected ConvexHull convexHull;
  protected PointCloud unvalidPrcsdPnt;
  protected PointCloud pntsToProcess;
  protected SortedPoint beingPrcsdPoint = new SortedPoint();
  protected ListIterator<SortedPoint> pntsToProcessIterator;

  Step() {
  }
  Step(ConvexHull cnvxHull) {
    this.convexHull = cnvxHull;
    this.unvalidPrcsdPnt = convexHull.getUnvalidProcessedPoints();
    this.pntsToProcess = convexHull.getPointsToProcess();
    this.pntsToProcessIterator = pntsToProcess.listIterator();
  }

  public abstract boolean update();
  public void show(){
    stroke(0, 126, 255);
    strokeWeight(18);
    if (pntsToProcessIterator.hasNext()) {
      point(beingPrcsdPoint.x, beingPrcsdPoint.y);
    }
  }
}
