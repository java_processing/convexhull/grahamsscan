class FindBottomByStep extends Step {

  protected float minY = Integer.MIN_VALUE;
  protected float Y;

  FindBottomByStep(ConvexHull cvxHull) {
    super(cvxHull);
  }

  public boolean update() {

    if (pntsToProcessIterator.hasNext() && !pntsToProcess.isEmpty()) {
      beingPrcsdPoint = pntsToProcessIterator.next();
      Y = beingPrcsdPoint.y;

      if (Y > minY) {
        minY = Y;
        convexHull.noErrorSet(0, beingPrcsdPoint);
      }
    } else {
      for (SortedPoint extremPoint : convexHull) {
        pntsToProcess.remove(extremPoint);
      }
      convexHull.setStep(new SortAnglesByStep(convexHull));
    }
    return false;
  }

  public void show() {
    super.show();

    stroke(255, 126, 0);
    strokeWeight(12);
    point(convexHull.get(0).x, convexHull.get(0).y);
  }
}
