class SortedPoint extends PVector implements Comparable<SortedPoint> {
  
  protected float angle;
  
  SortedPoint() {}
  SortedPoint(float x, float y) {
    super(x, y);
  }
  SortedPoint(float x, float y, float z) {
    super(x, y, z);
  }
  
  public void setAngle(SortedPoint btmPoint) {
    this.angle = PVector.angleBetween(new PVector(1,0),PVector.sub(this, btmPoint));
  }
  
  public void setAngle(SortedPoint beginPoint, SortedPoint endPoint) {
    this.angle = PVector.angleBetween(PVector.sub(endPoint, beginPoint),PVector.sub(this, beginPoint));
  }
  
  public float getAngle() {
    return this.angle;
  }
  
  public int compareTo(SortedPoint pnt) {
    return new Float(this.angle).compareTo(pnt.getAngle());
  }
}
