class SortAnglesByStep extends Step {

  protected PointCloud sortedPointsToProcess = new PointCloud();
  protected int index = 0;
  protected int size;
  protected boolean isAnimated;

  SortAnglesByStep(ConvexHull cvxHull) {
    super(cvxHull);
    this.isAnimated = this.convexHull.getIsAnimated();
    while (pntsToProcessIterator.hasNext()) {
      beingPrcsdPoint = pntsToProcessIterator.next();
      beingPrcsdPoint.setAngle(convexHull.get(0));
      this.sortedPointsToProcess.add(beingPrcsdPoint);
    }
    Collections.sort(this.sortedPointsToProcess);
    this.pntsToProcess = this.sortedPointsToProcess;
    this.convexHull.setPointsToProcess(this.pntsToProcess);
    this.size = this.pntsToProcess.size();
  }
  public boolean update() {
    if (index >= size || !isAnimated) {
      convexHull.setStep(new GrahamScanByStep(convexHull));
      return false;
    } else {
      return false;
    }
  }

  public void show() {
    super.show();
    stroke(0, 126, 255);
    strokeWeight(5);

    strokeWeight(10);
    colorMode(HSB, 1.5*size, 1, 1);
    for (int i = 0; i < index && i < size; i++) {
      stroke(i, 1, 1);
      SortedPoint pnt1;
      try {
        pnt1 = pntsToProcess.get(i-1);
      } catch (ArrayIndexOutOfBoundsException e) {
        pnt1 = pntsToProcess.get(i);
      }
      SortedPoint pnt2 = pntsToProcess.get(i);
      point(pnt1.x, pnt1.y);
      strokeWeight(4);
      connect(pnt1, pnt2);
      strokeWeight(18);
    }
    index++;
    colorMode(RGB, 255, 255, 255);
  }
}
