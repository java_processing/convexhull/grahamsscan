enum EnumSide {
  Left, Right;
  
  public static double getSin(SortedPoint beginPoint, SortedPoint endPoint, SortedPoint point) {
    PVector vectHull = PVector.sub(beginPoint, endPoint).normalize();
    PVector vectPrcsdPoint = PVector.sub(endPoint, point);
    return (vectHull.cross(vectPrcsdPoint)).z / SortedPoint.dist(beginPoint, point);
  }

  public static EnumSide whichSide(SortedPoint beginPoint, SortedPoint endPoint, SortedPoint point) {
    if (getSin(beginPoint, endPoint, point) <= 0) {
      return EnumSide.Left;
    } else {
      return EnumSide.Right;
    }
  }
}
