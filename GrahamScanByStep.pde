class GrahamScanByStep extends Step {

  protected SortedPoint prevP;
  protected SortedPoint nextP;
  protected boolean notFinished = true;
  
  GrahamScanByStep(ConvexHull cvxHull) {
    super(cvxHull);
    
    this.convexHull.add(pntsToProcess.get(0));
    this.convexHull.add(pntsToProcess.get(1));
    this.pntsToProcess.remove(0);
    this.pntsToProcess.remove(0);
    this.setTriplet();
  }

  public boolean update() {
    
    if (notFinished) {
      
      EnumSide sideOfNextPoint = EnumSide.whichSide(prevP, beingPrcsdPoint, nextP);
      
      if (sideOfNextPoint == EnumSide.Left) {
        this.nextPoint();
      } else {
        this.popPoint();
      }
      return false;
    } else {
      return true;
    } //<>//
  }

  public void setTriplet() {
    int size = convexHull.size();
      this.prevP = this.convexHull.get(size-3);
      this.beingPrcsdPoint = this.convexHull.get(size-2);
      this.nextP = this.convexHull.get(size-1); 
  }
  
  private void nextPoint() {
    try {
      this.convexHull.add(this.pntsToProcess.get(0));      
      this.pntsToProcess.remove(0);
    } catch (IndexOutOfBoundsException e) {
      notFinished = false;
    }

    this.setTriplet();
  }
  
  private void popPoint() {
    this.unvalidPrcsdPnt.add(beingPrcsdPoint);
    this.convexHull.remove(beingPrcsdPoint);
    this.setTriplet();
  }

  public void show() {
    super.show();
    stroke(0, 126, 255);
    strokeWeight(3);
    
    connect(this.prevP, this.beingPrcsdPoint); //<>//
    connect(this.beingPrcsdPoint, this.nextP);
  }
}
