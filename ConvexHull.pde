class ConvexHull extends PointCloud {

  private PointCloud initSetPoints;
  private PointCloud unvalidProcessedPoints;
  private PointCloud pointsToProcess;
  private Step currentStep;
  private boolean isFinished = false;
  private boolean isAnimated;
  private boolean printMessage = true;

  ConvexHull(PointCloud pntsToPrcs, boolean isAnim) {   
    ConvexHullConstructor(pntsToPrcs, isAnim);
  }
  ConvexHull(PointCloud pntsToPrcs) {
    ConvexHullConstructor(pntsToPrcs, true);
  }

  public void ConvexHullConstructor(PointCloud pntsToPrcs, boolean isAnim) {
    this.setBeginNanoTime();
    this.unvalidProcessedPoints = new PointCloud();
    this.pointsToProcess = pntsToPrcs;
    this.initSetPoints = (PointCloud) pointsToProcess.clone();
    this.currentStep = new FindBottomByStep(this);
    this.isAnimated = isAnim;
    
    if (!isAnimated) {
      //while(!this.update());
      while(!isFinished) {
        this.update();
      }
    }
  }
  
  public boolean getIsFinished() {
    return isFinished;
  }

  public boolean update() {
    if (!isFinished) {
      isFinished = currentStep.update();
    } 
    if (isFinished && printMessage) {
      this.setEndNanoTime();
      for (SortedPoint lastPoints : pointsToProcess) {
        unvalidProcessedPoints.add(lastPoints);
      }
      //pointsToProcess.clear();
      println("Convex Hull finished !");
      println("Size of the Convex Hull: " + this.size());
      this.printDuration();  
      printMessage = false;
    }
    return isFinished;
  }

  public void setPointsToProcess(PointCloud pnts) {
    this.pointsToProcess = pnts;
  }

  public PointCloud getPointsToProcess() {
    return pointsToProcess;
  }

  public PointCloud getUnvalidProcessedPoints() {
    return unvalidProcessedPoints;
  }
  
  public boolean getIsAnimated() {
   return this.isAnimated;
  }

  protected void show() {
    background(0);
    initSetPoints.show(12, 255, 255, 255);
    unvalidProcessedPoints.show(12, 126, 126, 126);
    strokeWeight(4);
    stroke(255, 126, 0);
    this.connectAll();
    currentStep.show();
    super.show(6, 126, 255, 0);
    if (isFinished) {
      //noLoop();
    }
  }

  public void setStep(Step stp) {
    currentStep = stp;
  }
  
  @Override
  public void printDuration() {
    println("Duration (us): " + this.getNanoDuration() / 1000);
  }
}
